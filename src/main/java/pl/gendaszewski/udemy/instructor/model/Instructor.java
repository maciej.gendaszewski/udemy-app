package pl.gendaszewski.udemy.instructor.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Instructor {

    private Integer id;
    private String name;
    private String surname;
    private String email;
    private LocalDate creationDate;
}
