package pl.gendaszewski.udemy.instructor.service;

import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;
import pl.gendaszewski.udemy.instructor.model.Instructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class InstructorService {

    private final List<Instructor> instructorList = new ArrayList<>();


    @PostConstruct
    private void init() {
        instructorList.add(new Instructor(1, "Marek", "M", "m@m.pl", LocalDate.now()));
        instructorList.add(new Instructor(2, "Maciej", "G", "m@g.pl", LocalDate.now()));
        instructorList.add(new Instructor(3, "Ola", "Q", "a@l.pl", LocalDate.now()));
        instructorList.add(new Instructor(4, "Kasia", "K", "m@g.pl", LocalDate.now()));
    }

    public List<Instructor> getInstructorList() {
        return instructorList;
    }

    public Instructor getInstructorById(Integer id) {
        return instructorList.stream()
                .filter(instructor -> instructor.getId().equals(id))
                .findFirst()
                .orElse(null);
    }


}
