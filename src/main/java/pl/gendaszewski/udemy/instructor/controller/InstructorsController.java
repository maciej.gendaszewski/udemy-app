package pl.gendaszewski.udemy.instructor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.gendaszewski.udemy.instructor.model.Instructor;
import pl.gendaszewski.udemy.instructor.service.InstructorService;

import java.util.List;

@RestController
@RequestMapping("/instructors")
public class InstructorsController {

    @Autowired
    InstructorService instructorService;


    @GetMapping()
    public List<Instructor> getAllInstructors() {
        return instructorService.getInstructorList();
    }

    @GetMapping("/{id}")
    public Instructor getInstructorById(@PathVariable int id) {
        return instructorService.getInstructorById(id);
    }
}
