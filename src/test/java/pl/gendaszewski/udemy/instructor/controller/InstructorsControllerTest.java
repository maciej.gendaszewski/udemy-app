package pl.gendaszewski.udemy.instructor.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.gendaszewski.udemy.instructor.model.Instructor;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class InstructorsControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;

    @Test
    void shouldReturnAllInstructors() throws Exception {
        //given
        //when
        MvcResult mvcResult = mockMvc.perform(get("/instructors"))
                .andDo(log())
                .andExpect(status().is(OK.value()))
                .andReturn();
        //then
        List<Instructor> instructorList = mapper.readValue(mvcResult.getResponse().getContentAsString(), List.class);
        assertNotNull(instructorList);
        assertEquals(4, instructorList.size());
    }

    @Test
    void shouldReturnInstructor() throws Exception {
        //given
        //when
        MvcResult mvcResult = mockMvc.perform(get("/instructors/1"))
                .andDo(print())
                .andExpect(status().is(OK.value()))
                .andReturn();
        //then
        Instructor expected = new Instructor(1, "Marek", "M", "m@m.pl", LocalDate.now());

        Instructor instructor = mapper.readValue(mvcResult.getResponse().getContentAsString(), Instructor.class);
        assertNotNull(instructor);
        assertEquals(expected.getId(), instructor.getId());
        assertEquals(expected.getName(), instructor.getName());
        assertEquals(expected.getSurname(), instructor.getSurname());
        assertEquals(expected.getEmail(), instructor.getEmail());
        //todo test creationDate
    }


    @Test
    void shouldNotReturnInstructor() throws Exception {
        //given
        //when
        //then
        mockMvc.perform(get("/instructors/0"))
                .andDo(print())
                .andExpect(status().is(OK.value()))
                .andExpect(jsonPath("$").doesNotExist());
    }
}