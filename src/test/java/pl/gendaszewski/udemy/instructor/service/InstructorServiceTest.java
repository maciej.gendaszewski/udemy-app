package pl.gendaszewski.udemy.instructor.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.gendaszewski.udemy.instructor.model.Instructor;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class InstructorServiceTest {

    @Autowired
    private InstructorService underTest;


    @Test
    void shouldReturnAllInstructors() {
        assertEquals(4, underTest.getInstructorList().size());
    }

    @Test
    void shouldReturnExistingInstructor() {
        Instructor expected = new Instructor(1, "Marek", "M", "m@m.pl", LocalDate.now());
        Instructor actual = underTest.getInstructorById(1);
        assertNotNull(actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getSurname(), actual.getSurname());
        assertEquals(expected.getEmail(), actual.getEmail());
        //todo test creationDate
    }

    @Test
    void shouldReturnNullWhenInstructorDoNotExist() {
        Instructor actual = underTest.getInstructorById(0);
        assertNull(actual);
    }
}